import "./App.css";
import { Provider } from "react-redux"; // Provider el el constructor que gestiona el estado permitiendo que este disponible hacia abajo en el arbol de componentes
import store from "./redux/store";

import  Counter  from "./components/Counter";

export const Title = ({ content }) => {
  // { content } => Desestructuración de los props
  return <h2>{content}</h2>;
};

function App() {
  return (
    // Provider recibe el props store definido en el archivo con el mismo nombre store.js, el cual contiene la configuración inicial del estado
    <Provider store={store}> 
      <div className="App">
        <Title id="id-1" content="Tìtulo 1" />
        <span num={3}>Primero</span>
        <span num={"3"}>Segundo</span>

        <Counter/>
      </div>
    </Provider>
  );
}

export default App;
