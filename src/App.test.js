import App, { Title } from './App';
import { configure, mount, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

configure({ adapter: new Adapter() });

describe('Test switch', () => {
  test('Probando render init ', () => {
      const wrapper = shallow(<App/>);
      console.log(wrapper.html());
      const element = wrapper.find('[num=3]');
     expect(element.text()).toEqual('Primero');
     
  })
});

