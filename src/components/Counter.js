import React, { setState } from 'react';
import { connect } from 'react-redux';
import { decrementEvent, incrementEvent } from '../redux/actions/counterActions';
import { getFullName, getReverseFullName } from '../redux/actions/userActions'
const Counter = (props) => { // Declaraciòn del componente Counter el cual recibe como parametro los props y entre ellos las propiedades almacenadas en el state global como los metodos dispatch que vienen de los Action Reducer
    console.log(props);

    const getFullName = () => {
         props.reverseFullName();
    };
   
  

    return (
        <div>
            <h1>{ props.state.user.firstName }</h1>
            <h2>{ props.state.user.country }</h2>
            <h3>{ props.state.user.fullName }</h3>
            <h3>{ props.state.user.reverseName }</h3>
            <button onClick={ props.increment }>+</button>
            <button onClick={ props.decrement }>-</button>
            <h1>{ props.state.counter }</h1>
            <button onClick={ getFullName } >get name</button>
        </div>
    );
}

const mapStateToProps = (state) => { // Funcion callback que conbierte los parametros del state global a props que se pasan al componente
    return { 
        state
     }
}

const mapDispatchToProps = (dispatch) => { // Funcion callbak que combierte los metrodos Actions dispatch a props que se pasan al componente y ser utilizados como propiedades en scope del componente actual
    return {
        increment: () => dispatch(incrementEvent()),
        decrement: () => dispatch(decrementEvent()),
        fullName: () => dispatch(getFullName()),
        reverseFullName: () => dispatch(getReverseFullName())
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Counter); // connect es la libreria que permite conectar el componente con el store global de redux