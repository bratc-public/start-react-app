import React from 'react';
import List from './List';
import { configure, mount, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

configure({ adapter: new Adapter() });

describe('Test switch', () => {
    const listOptions = [
        {name: 'Fresas', id: 1},
        {name: 'Manzanas', id: 2},
        {name: 'Uvas', id: 3},
        {name: 'Sandias', id: 4}
    ]
    it('Validar nodos h1',() => {
     
        const wrapper = shallow(<List list={listOptions} title="Frutas" />);
        expect(wrapper.find('h1').exists()).toBe(true);
        
    });

    it('Validar nodos ul',() => {
     
        const wrapper = shallow(<List list={listOptions} title="Frutas" />);
        const elementUl = wrapper.find('ul');
        console.log(elementUl);
       // expect().toBe(true);
        
    });

    it('Validar nodos hijos',() => {
     
        const wrapper = shallow(<List list={listOptions} title="Frutas" />);
        const elementUl = wrapper.find('ul');
    
        expect(elementUl.childAt(2).text()).toBe('Uvas'); // childAt(2) encuentra el indice 2 del arreglo que gerena los hijos de <ul>, en este caso el indice 2 el el tercer li que contiene las uvas
        expect(elementUl.childAt(2).type()).toBe('li'); // valida el tag html que existe en el elemento hijo del indice 2 de la lista de elementos

    });

    it('Set props',() => {
     
        const wrapper = shallow(<List list={listOptions} title="Frutas" />);
        wrapper.setProps({ // Actualiza completamente el la propiedad list del componente
            list: [
                {name: 'Mango', id: 1}
            ]
        });
        expect(wrapper.find('ul').childAt(0).text()).toBe('Mango'); // childAt(2) encuentra el indice 2 del arreglo que gerena los hijos de <ul>, en este caso el indice 2 el el tercer li que contiene las uvas

    });



});