

const List = ({title, list}) => {
    return (
        <section>
            <h1>{title}</h1>
            <ul>
                {
                    list.map(item => (
                        <li key={item.id} >{item.name}</li>
                    ))
                }
            </ul>
        </section>
    );
}

export default List;