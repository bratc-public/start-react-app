/**
 * Metodos que se llamaran a ejecutar cuando sean invocados en el componente que los 
 * necesite,
 * Para este caso el componente Counter hace uso de los metodos convertidos a props
 * los cuales ejecuta y pasan a ser interpretados en el archivo reduxReducer.js, y donde
 * coincida el tipo {type: 'INCREMENTE'} {type: 'DECREMENTE'} este ejecutara la
 * funcion que este declarada en el switch case correspondiente.
 */

import { FULLNAME, REVERSE_FULLNAME } from "../constantsActions"

export const getFullName = () => {
    return { type: FULLNAME };
}

export const getReverseFullName = () => {
    return { type: REVERSE_FULLNAME };
}