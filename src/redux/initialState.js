const initialStateCounter = 0;
const initialStateUser = {
    firstName: 'Yohon Jairo',
    lastName: 'Bravo Castro',
    country: 'Colombia'
}
export {
    initialStateCounter,
    initialStateUser
}