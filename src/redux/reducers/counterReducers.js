
/**
 * Definicion de las funciones reducer las cuales gestionaran los procesos de cambio de estado 
 * de forma global.
 * Este metodo counterReducer se pasa a la configuración del state
 */

import { DECREMENT, INCREMENT } from "../constantsActions";
import { initialStateCounter } from "../initialState";

const counter = (state = initialStateCounter, action) => {
       switch(action.type) {
        case INCREMENT:
            return state + 1;
        case DECREMENT:
            return state - 1;    
        default: 
            return state;    
    }
}


export default counter;