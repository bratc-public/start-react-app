import { combineReducers } from 'redux';
import user from './userReducers';
import counter from './counterReducers';

export default combineReducers({
    user,
    counter
});
