
/**
 * Definicion de las funciones reducer las cuales gestionaran los procesos de cambio de estado 
 * de forma global.
 * Este metodo counterReducer se pasa a la configuración del state
 */
// Constantses 

import { FULLNAME, REVERSE_FULLNAME } from "../constantsActions";
import { initialStateUser } from "../initialState";



// Estad actual


const user = (state = initialStateUser, action ) => {
    switch(action.type) {
        case FULLNAME:
            return {
                ...state,
                fullName: `${state.firstName} ${state.lastName}`
            };
        case REVERSE_FULLNAME:
            return  {
                ...state,
                reverseName: `${state.lastName} ${state.firstName}`
            };
        default:
            return state;    
    }
}

export default user