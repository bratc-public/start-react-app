/**
 * Definicion y configuración del estado global el cual estara disponible para todos
 * los componentes hijos dentro de la estructura de arbol de la app React
 */

import { legacy_createStore as createStore } from "redux";
import rootCombinateReducers from './reducers';

const store = createStore(rootCombinateReducers)// Se pasa como parametro la funcion pura la cual es el reducer 

export default store;